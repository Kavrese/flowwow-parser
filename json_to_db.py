import psycopg2
from config_db import *
from json import loads

connection = psycopg2.connect(
    host=HOST,
    port=PORT,
    user=USER,
    password=PASSWORD,
    database=DATABASE
)
cursor = connection.cursor()

with open("data.json", mode="r", encoding="utf-8") as file:
    data = file.readlines()[0]
    json_dict = loads(data)
    for i, data in enumerate(json_dict):
        name = data["name"]
        price = data["price"]
        old_price = data["old_price"]
        rate = data["rate"]
        rate_count = data["rate_count"]
        img = data["img_url"]
        insert_sql = f"""INSERT INTO flowers (name, price, old_price, image, rate, count_rate) VALUES ('{name}', {price}, {old_price if old_price is not None else price}, '{img}', {rate}, {rate_count})"""
        cursor.execute(insert_sql)
        print(f"{i+1}/{len(json_dict)} complete !")
    connection.commit()

print("All complete !")
