import dataclasses
from json import dumps

def list_dataclass_to_list_dict(lst: list[dataclasses.dataclass]) -> list[dict]:
    return [{j: getattr(i, j) for j in i.__annotations__}  for i in lst]

def to_json(list_dataclass: list[dataclasses.dataclass]) -> str:
    data = list_dataclass_to_list_dict(list_dataclass)
    return dumps(data, ensure_ascii=False)

def save_json(json: str):
    with open("data.json", mode="w", encoding="utf-8") as file:
        file.write(json)
