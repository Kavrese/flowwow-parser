from parser import *
from json_helper import to_json, save_json

parser = Parser()
data = []
max_page = parser.get_max_page()
for page in range(1, max_page+1):
    parser.get_page(page)
    data.extend(parser.parse_page())
    save_json(to_json(data))
    print(f"{page}/{max_page} complete !")
print("All complete !")