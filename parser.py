import dataclasses
from selenium import webdriver
from selenium.webdriver.common.by import By

@dataclasses.dataclass
class Product:
    name: str
    price: int
    old_price: int
    img_url: str
    rate: float
    rate_count: int


class Parser:
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.origin_url = "https://flowwow.com/elektrostal/"
        self.get_origin_page()

    def get_origin_page(self):
        self.driver.get(self.origin_url)

    def get_products_elements(self):
        try:
            return self.driver.find_element(By.CLASS_NAME, "tab-content-products")\
                    .find_elements(By.CSS_SELECTOR, "a")
        except Exception as e:
            print(e)
            return self.get_products_elements()

    def check_available_next_page(self) -> bool:
        pagination_next_style = self.driver.find_element(By.CLASS_NAME, "pagination-next").get_attribute("style")
        return pagination_next_style != "display: none;"

    def get_next_page(self) -> bool:
        if self.check_available_next_page():
            next_href = self.driver.find_element(By.CLASS_NAME, "pagination-next").get_attribute("href")
            self.driver.get(next_href)
            return True
        return False

    def get_page(self, page: int):
        if page == 1:
            self.get_origin_page()
        else:
            self.driver.get(self.origin_url + f"page-{page}/")

    def parse_page(self) -> list[Product]:
        products_elements = self.get_products_elements()
        data = []
        for elem in products_elements:
            img_url = elem.find_element(By.CSS_SELECTOR, "img").get_attribute("src")
            name = elem.find_element(By.CLASS_NAME, "name").text
            price = int(elem.find_element(By.CLASS_NAME, "price").find_element(By.CSS_SELECTOR, "span").text)
            old_price = elem.find_element(By.CLASS_NAME, "old-price").text
            rate = float(elem.find_element(By.CLASS_NAME, "reviews-rating").text)
            rate_count = int(elem.find_element(By.CLASS_NAME, "reviews-count").text.replace(" тыc.", "000"))
            data.append(Product(name, price, int(old_price) if old_price != "" else None, img_url, rate, rate_count))
        return data

    def get_max_page(self) -> int:
        return int(self.driver.find_element(By.CLASS_NAME, "pagination-digits").find_elements(By.CSS_SELECTOR, "a")[-1].find_element(By.CSS_SELECTOR, "span").text)